﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RepasoExamen
{
    public partial class Form1 : Form
    {
        private GestionCantiones[] arrayCanciones;
        private int Ncanciones = 0;
        public Form1()
        {
            arrayCanciones = new GestionCantiones[50];
            InitializeComponent();
        }

    
        private void BotonAdd_Click(object sender, EventArgs e)
        {
         string tituloCancion = TextBoxTitulo.Text;
         string nombreAutor = TextBoxAutor.Text;


            if (!tituloCancion.Equals("") && !nombreAutor.Equals(""))
            {
                if (Ncanciones < 50)
                {
                    arrayCanciones[Ncanciones] = new GestionCantiones(tituloCancion, nombreAutor);

                    LabelCanciones.Text += arrayCanciones[Ncanciones].ToString() + "\n";
                    Ncanciones++;
                }
            }
        
        }

        private void BotonMove_Click(object sender, EventArgs e)
        {
            int poscionOrigen= int.Parse( PosicionOrigenTexbox.Text)-1;
            int posicionDestino= int.Parse(PosicionDestinoTexbox.Text)-1;

            GestionCantiones buffer = arrayCanciones[poscionOrigen];
            arrayCanciones[poscionOrigen] = arrayCanciones[posicionDestino];
            arrayCanciones[posicionDestino] = buffer;

            LabelCanciones.Text = "";
            for (int i = 0; i <= Ncanciones-1; i++)
            {
                
                LabelCanciones.Text += arrayCanciones[i].ToString() + "\n";
            }

        }

    }
}
