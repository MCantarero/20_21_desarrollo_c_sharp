﻿namespace RepasoExamen
{
    partial class Form1
    {

        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelCanciones = new System.Windows.Forms.Label();
            this.TextBoxTitulo = new System.Windows.Forms.TextBox();
            this.BotonAdd = new System.Windows.Forms.Button();
            this.BotonMove = new System.Windows.Forms.Button();
            this.TextBoxAutor = new System.Windows.Forms.TextBox();
            this.PosicionOrigenTexbox = new System.Windows.Forms.TextBox();
            this.PosicionDestinoTexbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelCanciones
            // 
            this.LabelCanciones.AutoSize = true;
            this.LabelCanciones.Location = new System.Drawing.Point(359, 300);
            this.LabelCanciones.Name = "LabelCanciones";
            this.LabelCanciones.Size = new System.Drawing.Size(0, 13);
            this.LabelCanciones.TabIndex = 0;
            this.LabelCanciones.Click += new System.EventHandler(this.label1_Click);
            // 
            // TextBoxTitulo
            // 
            this.TextBoxTitulo.Location = new System.Drawing.Point(67, 176);
            this.TextBoxTitulo.Name = "TextBoxTitulo";
            this.TextBoxTitulo.Size = new System.Drawing.Size(262, 20);
            this.TextBoxTitulo.TabIndex = 1;
            // 
            // BotonAdd
            // 
            this.BotonAdd.Location = new System.Drawing.Point(67, 212);
            this.BotonAdd.Name = "BotonAdd";
            this.BotonAdd.Size = new System.Drawing.Size(262, 41);
            this.BotonAdd.TabIndex = 2;
            this.BotonAdd.Text = "Añadir";
            this.BotonAdd.UseVisualStyleBackColor = true;
            this.BotonAdd.Click += new System.EventHandler(this.BotonAdd_Click);
            // 
            // BotonMove
            // 
            this.BotonMove.Location = new System.Drawing.Point(407, 212);
            this.BotonMove.Name = "BotonMove";
            this.BotonMove.Size = new System.Drawing.Size(262, 41);
            this.BotonMove.TabIndex = 3;
            this.BotonMove.Text = "Mover";
            this.BotonMove.UseVisualStyleBackColor = true;
            this.BotonMove.Click += new System.EventHandler(this.BotonMove_Click);
            // 
            // TextBoxAutor
            // 
            this.TextBoxAutor.Location = new System.Drawing.Point(67, 111);
            this.TextBoxAutor.Name = "TextBoxAutor";
            this.TextBoxAutor.Size = new System.Drawing.Size(262, 20);
            this.TextBoxAutor.TabIndex = 4;
            this.TextBoxAutor.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // PosicionOrigenTexbox
            // 
            this.PosicionOrigenTexbox.Location = new System.Drawing.Point(407, 111);
            this.PosicionOrigenTexbox.Name = "PosicionOrigenTexbox";
            this.PosicionOrigenTexbox.Size = new System.Drawing.Size(262, 20);
            this.PosicionOrigenTexbox.TabIndex = 5;
            // 
            // PosicionDestinoTexbox
            // 
            this.PosicionDestinoTexbox.Location = new System.Drawing.Point(407, 176);
            this.PosicionDestinoTexbox.Name = "PosicionDestinoTexbox";
            this.PosicionDestinoTexbox.Size = new System.Drawing.Size(262, 20);
            this.PosicionDestinoTexbox.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nombre del Autor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nombre de la Cancion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Guardar Canciones ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(479, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Mover Canciones ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(404, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Posicion Origen";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(404, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Posicion Destino";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(311, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "LISTA DE CANCIONES";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 528);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PosicionDestinoTexbox);
            this.Controls.Add(this.PosicionOrigenTexbox);
            this.Controls.Add(this.TextBoxAutor);
            this.Controls.Add(this.BotonMove);
            this.Controls.Add(this.BotonAdd);
            this.Controls.Add(this.TextBoxTitulo);
            this.Controls.Add(this.LabelCanciones);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelCanciones;
        private System.Windows.Forms.TextBox TextBoxTitulo;
        private System.Windows.Forms.Button BotonAdd;
        private System.Windows.Forms.Button BotonMove;
        private System.Windows.Forms.TextBox TextBoxAutor;
        private System.Windows.Forms.TextBox PosicionOrigenTexbox;
        private System.Windows.Forms.TextBox PosicionDestinoTexbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

