﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoExamen
{
    class GestionCantiones
    {
        private string autor;
        private string titulo;
        public GestionCantiones(string Nombre, string Autor)
        {
            autor = Autor;
            titulo = Nombre;
        }


        public override string ToString()
        {
            return autor + ": " + titulo;
        }

    }
}
