﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepasoExamenRecursivo
{
    class Program
    {
        private static int i = 0;
        private static int z = 1;
        private static int ndigitos = 0;
        static void Main(string[] args)
        {
            cadenaRecursiva("hola");
            vueltaNumero(125);


        }

        public static void cadenaRecursiva(String cadena)
        {
           
            if (i>cadena.Length-1){
                return ;
            }
           
            Console.Write(cadena[i++]+"\n");
           
            cadenaRecursiva(cadena);

        }

        public static void vueltaNumero(int numero)
        {
            if ( numero < z ) {
                return;
            }

            Console.Write(numero % 10);//    1   
            vueltaNumero(numero / 10);//  0
           

        }
        public static int Numinverso(int numero)// 1º calcualr n digitos
        {
            for (int i = 1; i < numero; i *= 10)
                ndigitos++;

            return 0;
        }
    }
}
